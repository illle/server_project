package sample;

import sample.data.Command;
import sample.data.Lobby;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class Client implements Runnable {

    private Controller c;
    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;

    private int clientID;

    private String clientIP;
    private Lobby lobby;

    //These are the 3 values that constitutes a Player object on the client side
    private int SQLID;
    private String username;
    private int gamePoints;

    ArrayList<String> commands;


    public Client(Controller c, Socket socket) {
        this.c = c;
        this.socket = socket;
        clientID = IDHelper.getClientID();
        clientIP = socket.getInetAddress().toString();
        username = "Not logged in";
        c.addClient(this);
        commands = new ArrayList<>();
        for (Command command : Command.values()) {
            commands.add(command.name());
        }
        gamePoints = 0;
    }

    @Override
    public void run() {
        try {
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
            out.flush();
            c.log("Client ID " + clientID + ": Streams setup!");
        } catch (IOException e) {
            c.log("Client ID " + clientID + ": Failed to set up streams!");
            e.printStackTrace();
        }

        while (true) {
            try {
                Command command = Command.stringToCommand((String) in.readObject());
                if (command != null) rerouteCommand(command);
                else c.log("Client ID " + clientID + ": Read input that wasn't a command in main loop. This should never happen.");
            } catch (Exception e) {
                disconnect();
                e.printStackTrace();
                break;
            }
        }
    }

    private void disconnect() {
        try {
            out.close();
            in.close();
            socket.close();
            c.removeClient(this);
            if (lobby != null) {
                lobby.removeClient(this);
                lobby = null;
                c.fireListChanges();
            }
        } catch (IOException e) {
            c.log("Client ID " + clientID + ": Something went wrong when disconnecting.");
            e.printStackTrace();
        }
    }

    public void sendCommand(Command command, Object data) {
        try {
            out.writeObject(command.toString());
            out.flush();
            if (data != null) {
                out.writeUnshared(data);
                out.flush();
                c.log("Client ID " + clientID + ": Command " + command.toString() + " sent, with data " + data + " to follow!");
            } else {
                c.log("Client ID " + clientID + ": Command " + command.toString() + " sent!");
            }

        } catch(IOException e) {
            c.log("Client ID " + clientID + ": Failed to send command " + command.toString() + "!");
            e.printStackTrace();
        }
    }

    public void sendCommand(Command command) {
        sendCommand(command, null);
    }


    public void sendObject(Object object) {
        try {
            out.writeUnshared(object);
            out.flush();
            c.log("Client ID " + clientID + ": Object " + object + "sent!");
        } catch (IOException e) {
            c.log("Client ID " + clientID + ": Failed to send object " + object + "!");
            e.printStackTrace();
        }
    }

    public Object readMessage() {
        try {
            Object message = in.readObject();
            c.log("Client ID " + clientID + ": Message " + message + " read!");
            return message;
        } catch (Exception e) {
            c.log("Client ID " + clientID + ": Failed to read message!");
            e.printStackTrace();
            return null;
        }
    }

    private void rerouteCommand(Command command) {
        switch (command) {
            case CREATE_LOBBY:
                c.createLobby(this);
                break;
            case JOIN_LOBBY:
                c.joinLobby(this);
                break;
            case ECHO:
                sendObject(readMessage().toString());
                break;
            case LEAVE_LOBBY:
                c.leaveLobby(this);
                break;
            case LOG_IN:
                c.logIn(this);
                break;
            case SIGN_UP:
                c.signUp(this);
                break;
        }
    }


    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public String getRoomCode() {
        if (lobby == null) return "Not in lobby";
        return lobby.getRoomCode();

    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public boolean getIsHosting() {
        return (lobby != null) ? (lobby.getHost() == this) : false;
    }


    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public int getSQLID() {
        return SQLID;
    }

    public void setSQLID(int SQLID) {
        this.SQLID = SQLID;
    }

    public int getGamePoints() {
        return gamePoints;
    }

    public void setGamePoints(int gamePoints) {
        this.gamePoints = gamePoints;
    }
}
