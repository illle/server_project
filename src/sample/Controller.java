package sample;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sample.data.Command;
import sample.data.Lobby;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;


public class Controller {

    private ServerThread serverThread;
    private Connection sqlConn;

    @FXML
    private TextArea logArea;

    @FXML
    private TableView lobbyTable, clientTable;

    public void btnStartServerAL() {
        if (serverThread == null) serverThread = new ServerThread(this);
        serverThread.start();
    }

    public void establishSQLConnection() {
        try {
            Class.forName("org.h2.Driver");
            sqlConn = DriverManager.getConnection("jdbc:h2:~/partyUsers", "illle", "admin");
            log("C.getSQLConnection: Connection established");
        } catch (Exception e) {
            log("C.getSQLConnection: Connection failed");
            e.printStackTrace();
        }
    }


    public void addClient(Client client) {
        clientTable.getItems().add(client);
    }

    public void removeClient(Client client) {
        clientTable.getItems().remove(client);
    }

    public void addLobby(Lobby lobby) {
        lobbyTable.getItems().add(lobby);
    }

    public void removeLobby(Lobby lobby) {
        lobbyTable.getItems().remove(lobby);
    }

    public void log(String log) {
        Platform.runLater(() -> {
            logArea.appendText(log + "\n");
        });

    }


    public void createLobby(Client client) {
        Lobby lobby = new Lobby(client);
        addLobby(lobby);
        //Automatically deletes lobby if there's no clients left inside.
        lobby.getPlayerCountBinding().addListener((obs, oldV, newV) -> {
            if (newV.intValue() < 1) removeLobby(lobby);
        });
        client.setLobby(lobby);
        fireListChanges();
        log("Client ID " + client.getClientID() + ": Lobby created!");
    }

    public void joinLobby(Client client) {
        try {
            String roomCode = (String) client.readMessage();
            JSONObject resultData = new JSONObject();
            for (Object object : lobbyTable.getItems()) {
                Lobby lobby = (Lobby) object;   // ToDo: This is quite stupid. There's probably some way to declare data type in FXML so I don't have to cast.
                if (lobby.getRoomCode().equals(roomCode)) {
                    lobby.addClient(client);
                    client.setLobby(lobby);
                    fireListChanges();

                    JSONArray playerList = null;
                    try {
                        playerList = getJSONPlayerArray(lobby.getClients());
                        for (Client cl : lobby.getClients()) {
                            if (cl != client) {
                                cl.sendCommand(Command.UPDATE_PLAYER_LIST, playerList);
                            }
                        }
                    } catch (JSONException e) {
                        log("Client ID " + client.getClientID() + ": Exception when trying to make playerlist into a JSONArray;");
                        e.printStackTrace();
                    }


                    resultData.put("result", true);
                    resultData.put("playerList", playerList);
                    client.sendCommand(Command.JOIN_LOBBY, resultData);
                    log("Client ID " + client.getClientID() + ": Joined lobby with code " + lobby.getRoomCode() + ". Send updated players list to all clients.");

                    return;
                }
            }
            resultData.put("result", false);
            client.sendCommand(Command.JOIN_LOBBY, resultData);
            log("Client ID " + client.getClientID() + ": Failed to join lobby. No lobby with that room code exists.");

        } catch (JSONException e) {
            log("Client ID " + client.getClientID() + ": JSONException when trying to join jobby.");
            e.printStackTrace();
        }


    }

    private JSONArray getJSONPlayerArray(ArrayList<Client> clients) throws JSONException {

        JSONArray array = new JSONArray();
        for (Client cl : clients) {
            JSONObject object = new JSONObject();
            object.put("ID", cl.getSQLID());
            object.put("username", cl.getUsername());
            object.put("points", cl.getGamePoints());
            array.put(object);
        }
        return array;

    }

    private JSONObject getJSONPLayer(Client client) throws JSONException {
        JSONObject player = new JSONObject();
        player.put("ID", client.getSQLID());
        player.put("username", client.getUsername());
        player.put("points", client.getGamePoints());

        return player;

    }

    public void leaveLobby(Client client) {
        if (client.getLobby() != null) {
            client.getLobby().removeClient(client);
            client.setLobby(null);
        }
        fireListChanges();
        log("Client ID " + client.getClientID() + ": Lobby left!");

    }

    public void logIn(Client client) {
        if (sqlConn == null) establishSQLConnection();
        HashMap<String, Object> data = (HashMap<String, Object>) client.readMessage();
        String usernameOrEmail = (String) data.get("usernameOrEmail");
        String password = (String) data.get("password");
        String SQL = "SELECT * FROM REGISTERED_USERS WHERE (USERNAME = '" + usernameOrEmail + "' AND PASSWORD = '" + password + "')" +
                "OR (USERNAME = '" + usernameOrEmail + "' AND PASSWORD = '" + password + "')";
        try {
            Statement stmt = sqlConn.createStatement();
            ResultSet rs = stmt.executeQuery(SQL);
            JSONObject resultData = new JSONObject();
            if (rs.next()) {
                int ID = rs.getInt(1);
                String username = rs.getString(2);
                client.setUsername(username);
                client.setSQLID(ID);
                JSONObject player = getJSONPLayer(client);

                resultData.put("result", true);
                resultData.put("player", player);
                client.sendCommand(Command.LOG_IN, resultData);
            } else {
                resultData.put("result", false);
                client.sendCommand(Command.LOG_IN, resultData);
            }

        } catch (SQLException e) {
            log("Client ID " + client.getClientID() + ": SQLException when trying to log in.");
            e.printStackTrace();
        } catch (JSONException e) {
            log("Client ID " + client.getClientID() + ": JSONException when trying to log in.");
            e.printStackTrace();
        }
    }

    public void signUp(Client client) {
        if (sqlConn == null) establishSQLConnection();
        HashMap<String, Object> data = (HashMap<String, Object>) client.readMessage();
        String username = (String) data.get("username");
        String email = (String) data.get("email");
        String password = (String) data.get("password");
        String SQL = "INSERT INTO REGISTERED_USERS VALUES(DEFAULT, '" + username + "', '" + email + "', '" + password + "')";
        int insertedRecords = 0;
        Statement stmt;
        boolean usernameInUse = false;
        boolean emailInUse = false;
        try {
            stmt = sqlConn.createStatement();
            insertedRecords = stmt.executeUpdate(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                stmt = sqlConn.createStatement();
                SQL = "SELECT * FROM REGISTERED_USERS WHERE USERNAME = '" + username + "'";
                usernameInUse = stmt.executeQuery(SQL).first();
                SQL = "SELECT * FROM REGISTERED_USERS WHERE EMAIL = '" + email + "'";
                emailInUse = stmt.executeQuery(SQL).first();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

        JSONObject returnData = new JSONObject();
        try {
            if (insertedRecords > 0) {
                returnData.put("result", true);
                client.sendCommand(Command.SIGN_UP, returnData);
            } else {
                returnData.put("result", false);
                returnData.put("usernameInUse", usernameInUse);
                returnData.put("emailInUse", emailInUse);
                client.sendCommand(Command.SIGN_UP, returnData);
            }
        } catch (JSONException e) {
            log("Client ID " + client.getClientID() + ": JSONException when trying to respond to sign in request.");
            e.printStackTrace();
        }

    }



    /**
     * This might be fucking retarded. Used for when an inner value of an object in the table-lists is changed.
     * ObservableLists does not notice this, which means the tables does not get updated. Which means you have to force
     * it by calling this.
     */
    public void fireListChanges() {
        if (lobbyTable.getItems().size() > 0) lobbyTable.getItems().set(0, lobbyTable.getItems().get(0));
        if (clientTable.getItems().size() > 0) clientTable.getItems().set(0, clientTable.getItems().get(0));
    }
}
