package sample;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class IDHelper {

    private static int clientID = 0;
    private static int playerID = 0;
    private static ArrayList<String> listRoomCode = new ArrayList<>();


    public static int getClientID() {
        clientID ++;
        return clientID;
    }


    public static int getPlayerID() {
        playerID ++;
        return playerID;
    }

    public static String generateRoomCode() {
        String roomCode = "";
        for (int i = 0; i < 6; i++) {
            boolean letterOrNumber = ThreadLocalRandom.current().nextBoolean();
            if (letterOrNumber) {
                char notO = (char) (ThreadLocalRandom.current().nextInt(26) + 'a');
                while (notO == 'o') {
                    notO = (char) (ThreadLocalRandom.current().nextInt(26) + 'a');
                }
                roomCode += notO;
            }
            else {
                int not0 = ThreadLocalRandom.current().nextInt(9);
                not0 ++;
                roomCode += not0;
            }
        }
        roomCode = roomCode.toUpperCase();
        listRoomCode.add(roomCode);
        return roomCode;
    }



    public static ArrayList<String> getListRoomCode() {
        return listRoomCode;
    }

    public static void setListRoomCode(ArrayList<String> listRoomCode) {
        IDHelper.listRoomCode = listRoomCode;
    }
}
