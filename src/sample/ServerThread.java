package sample;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerThread extends Thread {

    private Controller c;
    ServerSocket server;

    public ServerThread(Controller c) {
        this.c = c;
    }

    @Override
    public void run() {
        try {
            server = new ServerSocket(6789, 100);
        } catch (IOException e){
            c.log("ServerThread: Failed to configure server");
            e.printStackTrace();
        }

        while (true) {
            try {
                c.log("Ready to accept a new client...");
                new Thread(new Client(c, server.accept())).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }



}
