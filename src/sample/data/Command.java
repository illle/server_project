package sample.data;

public enum Command {
    CREATE_LOBBY, JOIN_LOBBY, EXTRACT_MESSAGE, ECHO, LOG_IN, SIGN_UP, LEAVE_LOBBY, UPDATE_PLAYER_LIST;


    public static Command stringToCommand(String string) {
        for (Command c : Command.values()) {
            if (c.toString().equals(string)) return c;
        }
        return null;
    }

    public static Command intToCommand(int i) {
        return Command.values()[i];
    }

    public static int commandToInt(Command command) {
        for (int i = 0; i < Command.values().length; i++) {
            if (Command.values()[i] == command) return i;
        }
        return -1;
    }


}
