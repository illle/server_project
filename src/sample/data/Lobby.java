package sample.data;

import javafx.beans.binding.IntegerBinding;
import sample.Client;
import sample.IDHelper;

import java.util.ArrayList;

public class Lobby {

    private String hostID;
    private String roomCode;
    private IntegerBinding playerCountBinding;
    private int playerCount;
    private ArrayList<Client> clients;
    private Client host;

    public Lobby(Client host) {
        this.host = host;
        clients = new ArrayList<>();
        clients.add(host);
        roomCode = IDHelper.generateRoomCode();

    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    public boolean removeClient(Client c) {
        return clients.remove(c);
    }

    public void addClient(Client c) {
        clients.add(c);
    }

    public String getRoomCode() {
        return roomCode;
    }

    public String getHostID() {
        return hostID;
    }

    public void setHostID(String hostID) {
        this.hostID = hostID;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public IntegerBinding getPlayerCountBinding() {
        return playerCountBinding;
    }

    public void setPlayerCountBinding(IntegerBinding playerCountBinding) {
        this.playerCountBinding = playerCountBinding;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }


    public Client getHost() {
        return host;
    }

    public void setHost(Client host) {
        this.host = host;
    }
}
